package katas.fizzbuzz;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FizzBuzzTest2 {
    //Divisible par 3 = Fizz
    //Divisible par 5 = Buzz
    //Divisible par 3 et 5 = FizzBuzz
    //Le nombre sinon
    @ParameterizedTest
    @ValueSource(ints = {3})
    public void when_divisible_by_3_only_then_fizz(int number) {
        String actual = FizzBuzzApplicationTraining.compute(number);
        Assertions.assertThat(actual).isEqualTo("Fizz");
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10, 20})
    public void when_divisible_by_5_only_then_buzz(int number) {
        String actual = FizzBuzzApplicationTraining.compute(number);
        Assertions.assertThat(actual).isEqualTo("Buzz");
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 4, 11, 37})
    public void when_not_divisible_by_5_or_3_then_buzz(int number) {
        String actual = FizzBuzzApplicationTraining.compute(number);
        Assertions.assertThat(actual).isEqualTo(String.valueOf(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {15, 30, 45})
    public void when_divisible_by_3_and_5_then_fizzbuzz(int number) {
        String actual = FizzBuzzApplicationTraining.compute(number);
        Assertions.assertThat(actual).isEqualTo("FizzBuzz");
    }
}
