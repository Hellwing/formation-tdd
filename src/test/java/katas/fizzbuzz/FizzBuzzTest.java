package katas.fizzbuzz;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FizzBuzzTest {
    //Divisible par 3 = Fizz
    //Divisible par 5 = Buzz
    //Divisible par 3 et 5 = FizzBuzz
    //Le nombre sinon

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 4})
    public void when_not_divisible_by_3_5_then_number(int number) {
        //Given
        //When
        String resultat = FizzBuzzApplication.fizzBuzz(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo(Integer.toString(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 6, 9})
    public void when_divisible_by_3_then_fizz(int number) {
        //Given
        //When
        String resultat = FizzBuzzApplication.fizzBuzz(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("Fizz");
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10})
    public void when_divisible_by_5_then_buzz(int number) {
        //Given
        //When
        String resultat = FizzBuzzApplication.fizzBuzz(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("Buzz");
    }

    @ParameterizedTest
    @ValueSource(ints = {15, 30, 60})
    public void when_divisible_by_15_then_fizzbuzz(int number) {
        //Given
        //When
        String resultat = FizzBuzzApplication.fizzBuzz(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("FizzBuzz");
    }
}
