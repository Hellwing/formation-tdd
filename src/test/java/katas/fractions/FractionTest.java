package katas.fractions;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
/*
 * Kata : Addition de deux fractions irréductibles
 */
public class FractionTest {

    @Test
    public void when_fraction_is_0_then_numerator_is_0() throws Exception {
        Fraction zero = new Fraction(0, 1);
        assertThat(zero.getNumerator()).isEqualTo(0);
    }

    @Test
    public void when_fraction_is_1_then_numerator_is_1() throws Exception {
        Fraction one = new Fraction(1, 1);
        assertThat(one.getNumerator()).isEqualTo(1);
    }

    @Test
    public void when_fraction_is_one_half_then_denominator_is_2() throws Exception {
        Fraction half = new Fraction(1, 2);

        assertThat(half.getDenominator()).isEqualTo(2);
    }

    @Test
    public void when_fraction_is_one_third_then_denominator_is_2() throws Exception {
        Fraction oneThird = new Fraction(1, 3);

        assertThat(oneThird.getDenominator()).isEqualTo(3);
    }

    @Test
    public void when_denominator_is_0_then_throws_division_by_zero() {
        //assertThatThrownBy(() -> new Fraction(1, 0)).isInstanceOf(ZeroDenominatorException.class);
    }

    @Test
    public void when_fraction_is_2_over_6_then_result_is_one_third() throws Exception {
        Fraction twoSixth = new Fraction(2, 6);

        assertFraction(twoSixth, 1, 3);
    }

    @Test
    public void when_fraction_is_9_over_12_then_result_is_three_quarters() throws Exception {
        Fraction nineOverTwelve = new Fraction(9, 12);

        assertFraction(nineOverTwelve, 3, 4);
    }

    @Test
    public void when_adding_one_third_and_one_third_then_result_is_two_third() throws Exception {
        Fraction oneThird = new Fraction(1, 3);

        Fraction actual = oneThird.add(oneThird);

        assertFraction(actual, 2, 3);
    }

    @Test
    public void when_adding_two_third_and_one_half_then_result_is_seven_over_six() throws Exception {
        Fraction twoThird = new Fraction(2, 3);
        Fraction oneHalf = new Fraction(1, 2);

        Fraction actual = twoThird.add(oneHalf);

        assertFraction(actual, 7, 6);
    }


    @Test
    public void when_adding_3_over_5_and_3_over_4_then_result_is_27_over_20() throws Exception {
        Fraction twoThird = new Fraction(3, 5);
        Fraction oneHalf = new Fraction(3, 4);

        Fraction actual = twoThird.add(oneHalf);

        assertFraction(actual, 27, 20);
    }

    @Test
    public void when_adding_1_over_2_and_1_over_6_then_result_is_2_over_3() throws Exception {
        Fraction twoThird = new Fraction(1, 2);
        Fraction oneHalf = new Fraction(1, 6);

        Fraction actual = twoThird.add(oneHalf);

        assertFraction(actual, 2, 3);
    }

    private void assertFraction(Fraction actualFraction, int expectedNumerator, int expectedDenominator) {
        assertThat(actualFraction.getNumerator()).isEqualTo(expectedNumerator);
        assertThat(actualFraction.getDenominator()).isEqualTo(expectedDenominator);
    }
}