package katas.fractions;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class FractionTest2 {

    @Test
    public void when_0_then_numerator_is_0() {
        Fraction2 actual = new Fraction2(0, 1);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(0);
    }

    @Test
    public void when_0_then_denominator_is_1() {
        Fraction2 actual = new Fraction2(0, 1);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(1);
    }

    @Test
    public void when_1_then_numerator_is_1() {
        Fraction2 actual = new Fraction2(1, 1);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(1);
    }

    @Test
    public void when_2_then_numerator_is_2() {
        Fraction2 actual = new Fraction2(2, 1);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(2);
    }

    @Test
    public void when_one_half_then_denominator_is_2() {
        Fraction2 actual = new Fraction2(1, 2);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(2);
    }

    @Test
    public void when_one_third_then_denominator_is_3() {
        Fraction2 actual = new Fraction2(1, 3);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(3);
    }

    @Test
    public void when_two_quarter_then_numerator_is_1() {
        Fraction2 actual = new Fraction2(2, 4);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(1);
    }

    @Test
    public void when_two_quarter_then_denominator_is_2() {
        Fraction2 actual = new Fraction2(2, 4);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(2);
    }

    @Test
    public void when_six_ninth_then_numerator_is_2() {
        Fraction2 actual = new Fraction2(6, 9);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(2);
    }

    @Test
    public void when_six_ninth_then_denominator_is_3() {
        Fraction2 actual = new Fraction2(6, 9);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(3);
    }


    @Test
    public void when_4_over_10_then_numerator_is_2() {
        Fraction2 actual = new Fraction2(4, 10);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(2);
    }

    @Test
    public void when_4_over_10_then_denominator_is_5() {
        Fraction2 actual = new Fraction2(4, 10);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(5);
    }


    @Test
    public void when_18_over_24_then_numerator_is_3() {
        Fraction2 actual = new Fraction2(18, 24);
        Assertions.assertThat(actual.getNumerator()).isEqualTo(3);
    }

    @Test
    public void when_18_over_24_then_denominator_is_4() {
        Fraction2 actual = new Fraction2(18, 24);
        Assertions.assertThat(actual.getDenominator()).isEqualTo(4);
    }

    @Test
    public void when_1_third_plus_1_quarter_then_7_over_12() {
        Fraction2 actual = new Fraction2(1, 3);
        Fraction2 addedFraction = new Fraction2(1, 4);

        Fraction2 actualResult = actual.add(addedFraction);

        Assertions.assertThat(actualResult.getNumerator()).isEqualTo(7);
        Assertions.assertThat(actualResult.getDenominator()).isEqualTo(12);

    }

    @Test
    public void when_1_third_plus_1_half_then_2_third() {
        Fraction2 actual = new Fraction2(1, 3);
        Fraction2 addedFraction = new Fraction2(1, 2);

        Fraction2 actualResult = actual.add(addedFraction);

        Assertions.assertThat(actualResult.getNumerator()).isEqualTo(5);
        Assertions.assertThat(actualResult.getDenominator()).isEqualTo(6);

    }


    @Test
    public void when_3_over_7_plus_4_over_3_then_37_over_21() {
        Fraction2 actual = new Fraction2(3, 7);
        Fraction2 addedFraction = new Fraction2(4, 3);

        Fraction2 actualResult = actual.add(addedFraction);

        Assertions.assertThat(actualResult.getNumerator()).isEqualTo(37);
        Assertions.assertThat(actualResult.getDenominator()).isEqualTo(21);

    }
}
