package katas.foobarqix;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FooBarQixTest {
    //Divisible par 3 = Foo
    //Divisible par 5 = Bar
    //Divisible par 7 = Qix
    //Avec les compositions
    //Le nombre sinon

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 4})
    public void when_not_divisible_by_3_5_7_then_number(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo(Integer.toString(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 6, 9})
    public void when_divisible_by_3_then_foo(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("Foo");
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10})
    public void when_divisible_by_5_then_bar(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("Bar");
    }

    @ParameterizedTest
    @ValueSource(ints = {15, 30, 60})
    public void when_divisible_by_15_then_foobar(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("FooBar");
    }

    @ParameterizedTest
    @ValueSource(ints = {7, 14})
    public void when_divisible_by_7_then_qix(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("Qix");
    }

    @ParameterizedTest
    @ValueSource(ints = {21, 42})
    public void when_divisible_by_3_and_7_then_fooqix(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("FooQix");
    }

    @ParameterizedTest
    @ValueSource(ints = {35, 140})
    public void when_divisible_by_5_and_7_then_fooqix(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("BarQix");
    }


    @ParameterizedTest
    @ValueSource(ints = {105, 210})
    public void when_divisible_by_3_and_5_and_7_then_foobarqix(int number) {
        //Given
        //When
        String resultat = FooBarQixApplication.fooBarQix(number);
        //Then
        Assertions.assertThat(resultat).isEqualTo("FooBarQix");
    }
}
