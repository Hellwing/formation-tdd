package katas.foobarqix;

import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FooBarQixV2Test {
    //V2 : Mêmes règles que FoobarQix, mais :
    //Divisibilité du nombre, puis Foo, Bar et Qix pour chaque chiffre 3, 5, ou 7 présent dans le nombre
    //3 : FooFoo
    //5 : BarBar
    //7 : QixQix
    //537 : FooBarFooQix
    //17 : 17Qix

    @Test
    public void when_3_then_foofoo() {
        String actual = FooBarQixApplication.fooBarQixV2(3);
        assertThat(actual).isEqualTo("FooFoo");
    }

    @Test
    public void when_6_then_foo() {
        String actual = FooBarQixApplication.fooBarQixV2(6);
        assertThat(actual).isEqualTo("Foo");
    }

    @Test
    public void when_5_then_barbar() {
        String actual = FooBarQixApplication.fooBarQixV2(5);
        assertThat(actual).isEqualTo("BarBar");
    }

    @Test
    public void when_15_then_barbar() {
        String actual = FooBarQixApplication.fooBarQixV2(15);
        assertThat(actual).isEqualTo("FooBarBar");
    }

    @Test
    public void when_7_then_qixqix() {
        String actual = FooBarQixApplication.fooBarQixV2(7);
        assertThat(actual).isEqualTo("QixQix");
    }

    @Test
    public void when_17_then_17Qix() {
        String actual = FooBarQixApplication.fooBarQixV2(17);
        assertThat(actual).isEqualTo("17Qix");
    }

    @Test
    public void when_537_then_foobarfooqix() {
        String actual = FooBarQixApplication.fooBarQixV2(537);
        assertThat(actual).isEqualTo("FooBarFooQix");
    }
}
