package katas.diamond;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class DiamondTest {
    //Etant donné une lettre, afficher de A à cette lettre en fore de diamant :
    //C ->  A
    //     B B
    //    C   C
    //     B B
    //      A
    //
    //A ->  A

    @Test
    public void when_A_then_return_A() {
        String actual = new Diamond('A').toString();
        Assertions.assertThat(actual).isEqualTo("A");
    }

    @Test
    public void when_B_then_return_AB() {
        String actual = new Diamond('B').toString();
        String expected = " A \nB B\n A ";
        System.out.println(actual);
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void when_C_then_return_ABC() {
        String actual = new Diamond('C').toString();
        String expected = "  A  \n B B \nC   C\n B B \n  A  ";
        System.out.println(actual);
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void when_D_then_return_ABCD() {
        String actual = new Diamond('D').toString();
        String expected = "   A   \n  B B  \n C   C \nD     D\n C   C \n  B B  \n   A   ";
        System.out.println(actual);
        Assertions.assertThat(actual).isEqualTo(expected);
    }


    //@Test
    public void explore() {
        String actual = new Diamond('Z').toString();
        System.out.println(actual);
    }
}
