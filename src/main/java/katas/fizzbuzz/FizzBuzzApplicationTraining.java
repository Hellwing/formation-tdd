package katas.fizzbuzz;

public class FizzBuzzApplicationTraining {
    public static String compute(int number) {
        String resultat = "";
        if (number % 3 == 0) {
            resultat += "Fizz";
        }
        if (number % 5 == 0) {
            resultat += "Buzz";
        }

        if (resultat.length() == 0) {
            resultat = String.valueOf(number);
        }
        return resultat;
    }
}
