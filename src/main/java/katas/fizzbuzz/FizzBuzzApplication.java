package katas.fizzbuzz;

public class FizzBuzzApplication {

    public static String fizzBuzz(int number) {
        String resultat = "";
        if (number % 3 == 0) {
            resultat = "Fizz";
        }
        if (number % 5 == 0) {
            resultat += "Buzz";
        }

        if (resultat.length() == 0) {
            resultat = Integer.toString(number);
        }
        return resultat;
    }
}
