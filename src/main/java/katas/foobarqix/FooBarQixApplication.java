package katas.foobarqix;

public class FooBarQixApplication {

    public static String fooBarQix(int number) {
        String resultat = "";
        if (isModuloBy(number, 3)) {
            resultat = "Foo";
        }
        if (isModuloBy(number, 5)) {
            resultat += "Bar";
        }
        if (isModuloBy(number, 7)) {
            resultat += "Qix";
        }

        if (resultat.length() == 0) {
            resultat = Integer.toString(number);
        }

        return resultat;
    }

    public static String fooBarQixV2(int number) {
        String resultat = fooBarQix(number);

        char[] numberDigits = String.valueOf(number).toCharArray();
        for (char digit: numberDigits) {
            resultat += computeDigit(digit);
        }

        return resultat;
    }

    private static boolean isModuloBy(int number, int modulo) {
        return number % modulo == 0;
    }

    private static String computeDigit(char digit) {
        if (digit == '3') {
            return "Foo";
        } else if (digit == '5') {
            return "Bar";
        } else if (digit == '7') {
            return "Qix";
        }
        return "";
    }
}
