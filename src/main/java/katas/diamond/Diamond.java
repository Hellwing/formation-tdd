package katas.diamond;

import java.util.ArrayList;
import java.util.List;

public class Diamond {
    private static String SPACE = " ";
    private static String NEW_LINE = "\n";

    private List<String> lines = new ArrayList<>();

    public Diamond(char letter) {
        int lettersNumber = letter - 'A';
        for (int i = 0; i <= lettersNumber; i++) {
            lines.add(getLetterLine((char) ('A' + i), letter));
        }
    }

    private void addSpacesInBuffer(StringBuffer buffer, int count) {
        for (int i = 0; i < count; i++) {
            buffer.append(SPACE);
        }
    }

    private String getLetterLine(char currentletter, char initialLetter) {
        StringBuffer lineBuffer = new StringBuffer();
        addSpacesInBuffer(lineBuffer, initialLetter - currentletter);
        lineBuffer.append(currentletter);
        if (currentletter != 'A') {
            addSpacesInBuffer(lineBuffer, 2 * (currentletter - 'B') + 1);
            lineBuffer.append(currentletter);
        }
        addSpacesInBuffer(lineBuffer, initialLetter - currentletter);
        return lineBuffer.toString();
    }

    public String toString(){
        StringBuffer buffer = new StringBuffer(lines.get(0));
        for (int i = 1; i < lines.size(); i++) {
            buffer.append(NEW_LINE).append(lines.get(i));
        }
        for (int i = lines.size() - 2; i >=0; i--) {
            buffer.append(NEW_LINE).append(lines.get(i));
        }
        return buffer.toString();
    }
}
