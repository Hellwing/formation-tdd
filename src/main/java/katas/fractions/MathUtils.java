package katas.fractions;

public class MathUtils {

    //Méthode fournie dans laquelle on a confiance. Inutile de la tester
    //De la même manière, on ne teste pas les méthodes de librairies importées
    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}
