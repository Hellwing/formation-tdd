package katas.fractions;

public class ZeroDenominatorException extends Exception {
    public ZeroDenominatorException() {
        super("The denominator can't be 0");
    }
}
