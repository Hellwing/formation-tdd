package katas.fractions;

public class Fraction2 {

    private int numerator;
    private int denominator;

    public Fraction2(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator/MathUtils.gcd(numerator, denominator);
    }

    public int getDenominator() {
        return denominator/MathUtils.gcd(numerator, denominator);
    }

    public Fraction2 add(Fraction2 addedFraction) {
        return new Fraction2(this.numerator * addedFraction.getDenominator() + this.denominator * addedFraction.getNumerator(),
                this.denominator * addedFraction.getDenominator());
    }
}
