package katas.fractions;

public final class Fraction {
    //TODO il faut des TU pour
    //- définir les attributs dont on a besoin (1 test pour numérateur, un test dénominateur)
    //- définir l'exception denominateur nul
    //- définir la notion d'irréductibilité (gcd)
    //  Etapes :  1. Plusieurs réductions naïves de fraction
    //            2. Pendant le refactor, identifier que les nombres d'entrée possèdent un diviseur commun pour atteindre les nombres attendus
    //
    //Méthode add à implémenter

    private int numerator;

    private int denominator;

    public Fraction(int numerator, int denominator) throws ZeroDenominatorException {
        if (denominator == 0) {
            throw new ZeroDenominatorException();
        }
        int gcd = MathUtils.gcd(numerator, denominator);
        this.numerator = numerator / gcd;
        this.denominator = denominator / gcd;
    }

    public int getNumerator() {
        return this.numerator;
    }

    public int getDenominator() {
        return this.denominator;
    }

    public Fraction add(Fraction other) throws Exception {
        return new Fraction(this.numerator * other.denominator + other.numerator * this.denominator,
                this.denominator * other.denominator);
    }
}
